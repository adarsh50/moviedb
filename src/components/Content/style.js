import { styled } from "@mui/system";
import { Box, Button, Card, Typography } from "@mui/material";

const HomeContent = styled(Box, { name: "HomeContent" })({
  marginTop: "20px",
  display: "flex",
});

export const HomeContentWrapper = styled(Box, { name: "HomeContentWrapper" })({
  margin: "0 96px",
  width: "100%",
  "@media (min-width: 0px) and (max-width: 768px)": {
    display: "flex",
    flexDirection: "column",
    margin: "0",
  },
});

export const HomeContentTvWrapper = styled(Typography, {
  name: "HomeContentTvWrapper",
})({
  marginLeft: "50px",
  "@media (min-width: 0px) and (max-width: 768px)": {
    textAlign: "center",
  },
});

export const HomeContentSection = styled(Box)({
  display: "flex",
  "@media (min-width: 0px) and (max-width: 768px)": {
    display: "flex",
    flexDirection: "column",
  },
});

export const HomeContentSectionWrapper = styled(Box)({
  width: "30%",
  "@media (min-width: 0px) and (max-width: 768px)": {
    display: "flex",
    flexDirection: "column",
    marginBottom: "15px",
    width: "90%",
    marginLeft: "20px",
  },
});

export const ListItemBox = styled(Box, { name: "ListItemBox" })({
  border: "1px solid #e3e3e3",
  borderRadius: "10px",
});

export const MoviesGridContainer = styled(Box)({
  marginLeft: "20px",
  width: "100%",
  "@media (min-width: 0px) and (max-width: 768px)": {
    margin: "0",
  },
});

export const MoviesCard = styled(Card)({
  height: "361px",
  width: "180px",
  borderRadius: "10px",
  objectFit: "cover",
  "@media (min-width: 0px) and (max-width: 768px)": {
    width: "90%",
    margin: "0 20px",
  },
});

export const MoviesTitle = styled(Typography)({
  fontSize: "16px",
  fontWeight: "700",
});

export const MoviesReleaseDate = styled(Typography)({
  fontSize: "1em",
  color: "rgba(0,0,0,0.6)",
});

export const MoviesLoadingContainer = styled(Box, {
  name: "MovieLoadingContainer",
})({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "100%",
});

export const MoviesLoadingContainerWrapper = styled(Typography, {
  name: "MovieLoadingContainerWrapper",
})({
  color: "blue",
});

export const SearchButton = styled(Button)({
  border: "1px solid #e3e3e3",
  borderRadius: "25px",
  color: "black",
  textAlign: "center",
  width: "100%",
  backgroundColor: "#ddd8d8",
});
export default HomeContent;
