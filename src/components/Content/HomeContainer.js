import React from "react";
import Home from "./Home";
import { useGetAllMoviesQuery } from "../../store/API/movies";
import { useState } from "react";

const HomeContainer = () => {
  const { data, isLoading, isSuccess } = useGetAllMoviesQuery();

  console.log({ isLoading, isSuccess });
  console.log({ data });
  const [open, setOpen] = useState(true);
  const [filter, setFilter] = useState(true);
  const [watch, setWatch] = useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  const handleFilter = () => {
    setFilter(!filter);
  };

  const handleWatch = () => {
    setWatch(!watch);
  };

  return (
    <Home
      handleClick={handleClick}
      handleFilter={handleFilter}
      handleWatch={handleWatch}
      filter={filter}
      watch={watch}
      open={open}
      data={data}
      isLoading={isLoading}
      isSuccess={isSuccess}
    />
  );
};

export default HomeContainer;
