import {
  Box,
  ListItemButton,
  List,
  Collapse,
  ListItemText,
  Grid,
  Card,
  CardContent,
  CardMedia,
} from "@mui/material";
import React from "react";
import { useState } from "react";
import HomeContent, {
  HomeContentWrapper,
  ListItemBox,
  HomeContentSection,
  HomeContentSectionWrapper,
  SearchButton,
  MoviesGridContainer,
  MoviesCard,
  MoviesTitle,
  MoviesReleaseDate,
  MoviesLoadingContainer,
  MoviesLoadingContainerWrapper,
  HomeContentTvWrapper,
} from "./style";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Navbar from "../../common/Header/Navbar";
const Home = ({
  data,
  isLoading,
  isSuccess,
  handleClick,
  handleFilter,
  handleWatch,
  open,
  filter,
  watch,
}) => {
  return (
    <>
      <Navbar />
      <HomeContent>
        <HomeContentWrapper>
          <HomeContentTvWrapper variant="h5">
            Popular Tv Shows
          </HomeContentTvWrapper>
          <br />
          <HomeContentSection>
            <HomeContentSectionWrapper>
              <ListItemBox>
                <ListItemButton onClick={handleClick}>
                  <ListItemText primary="Sort" />
                  {open ? <ExpandMoreIcon /> : <ChevronRightIcon />}
                </ListItemButton>
                <Collapse in={open} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItemButton sx={{ pl: 4 }}>
                      <ListItemText primary="Default" />
                    </ListItemButton>
                  </List>
                </Collapse>
              </ListItemBox>
              <br />
              <ListItemBox>
                <ListItemButton onClick={handleFilter}>
                  <ListItemText primary="Filter" />
                  {filter ? <ExpandMoreIcon /> : <ChevronRightIcon />}
                </ListItemButton>
                <Collapse in={filter} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItemButton sx={{ pl: 4 }}>
                      <ListItemText primary="Default" />
                    </ListItemButton>
                  </List>
                </Collapse>
              </ListItemBox>
              <br />
              <ListItemBox>
                <ListItemButton onClick={handleWatch}>
                  <ListItemText primary="Where to watch" />
                  {watch ? <ExpandMoreIcon /> : <ChevronRightIcon />}
                </ListItemButton>
                <Collapse in={watch} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItemButton sx={{ pl: 4 }}>
                      <ListItemText primary="Default" />
                    </ListItemButton>
                  </List>
                </Collapse>
              </ListItemBox>
              <br />
              <SearchButton>Search</SearchButton>
            </HomeContentSectionWrapper>
            {isLoading === false && isSuccess === true ? (
              <MoviesGridContainer>
                <Grid container spacing={3}>
                  {data?.results.map((el, index) => (
                    <Grid
                      item
                      xs={12}
                      xl={2.4}
                      lg={3}
                      md={4}
                      sm={6}
                      key={index}
                    >
                      <MoviesCard>
                        {" "}
                        <CardMedia
                          component="img"
                          height="241px"
                          image={`https://image.tmdb.org/t/p/original${el.backdrop_path}`}
                        />
                        <Box>
                          <CardContent>
                            <MoviesTitle>{el.title}</MoviesTitle>
                            <MoviesReleaseDate>
                              {el.release_date}
                            </MoviesReleaseDate>
                          </CardContent>
                        </Box>
                      </MoviesCard>
                    </Grid>
                  ))}
                </Grid>
              </MoviesGridContainer>
            ) : (
              <MoviesLoadingContainer>
                <MoviesLoadingContainerWrapper variant="h3">
                  Loading...................
                </MoviesLoadingContainerWrapper>
              </MoviesLoadingContainer>
            )}
          </HomeContentSection>
        </HomeContentWrapper>
      </HomeContent>
    </>
  );
};

export default Home;
