import React from "react";
import { AppBar, Toolbar, Typography } from "@mui/material";
import NavbarComponent, {
  NavbarWrapper,
  NavbarLeft,
  NavbarRight,
} from "./style";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import GTranslateIcon from "@mui/icons-material/GTranslate";
import SearchIcon from "@mui/icons-material/Search";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <NavbarComponent>
        <Toolbar>
          <NavbarWrapper>
            <NavbarLeft>
              <img src="https://www.themoviedb.org/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg" />
              <Link to="/movies">
                <Typography variant="h6">Movies</Typography>
              </Link>
              <Link to="/tvShows">
                <Typography variant="h6">Tv Shows</Typography>
              </Link>
            </NavbarLeft>
            <NavbarRight>
              <Link to="/">
                <AddCircleIcon />
              </Link>
              <Link to="/">
                <GTranslateIcon />
              </Link>

              <Typography variant="h6">Login</Typography>
              <Typography variant="h6">Join TMDB</Typography>
              <Link to="/">
                <SearchIcon />
              </Link>
            </NavbarRight>
          </NavbarWrapper>
        </Toolbar>
      </NavbarComponent>
    </>
  );
};

export default Navbar;
