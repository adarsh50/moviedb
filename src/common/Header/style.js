import { AppBar, Toolbar, Box } from "@mui/material";
import { styled } from "@mui/system";

const NavbarComponent = styled(AppBar)({
  backgroundColor: "rgba(3,37,65,1)",
  position: "static",
});

export const NavbarWrapper = styled(Box)({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "space-between",
  width: "100%",
  margin: "0 96px",
  "@media (min-width: 0px) and (max-width:768px)": {
    margin: "0",
    flexDirection: "column",
  },
});

export const NavbarLeft = styled(Box)({
  img: {
    width: "100px",
    marginRight: "20px",
  },
  display: "flex",
  h6: {
    marginRight: "20px",
  },
  a: {
    textDecoration: "none",
    color: "white",
  },
  "@media (min-width: 0px) and (max-width:768px)": {
    margin: "0",
    justifyContent: "space-between",
  },
});

export const NavbarRight = styled(Box)({
  display: "flex",
  h6: {
    marginRight: "20px",
  },
  a: {
    marginRight: "20px",
    marginTop: "5px",
    textDecoration: "none",
    color: "white",
  },
  "@media (min-width: 0px) and (max-width:768px)": {
    margin: "0",
    justifyContent: "space-between",
  },
});

export default NavbarComponent;
