import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const moviesApi = createApi({
  reducerPath: "moviesApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.themoviedb.org/" }),
  endpoints: (builder) => ({
    getAllMovies: builder.query({
      query: () =>
        "3/movie/popular?api_key=4fc0260b53a0c16883d96ff06d53752d&language=en-US&page=1",
    }),
  }),
});

export const { useGetAllMoviesQuery } = moviesApi;
