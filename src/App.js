import logo from "./logo.svg";
import "./App.css";
import Navbar from "./common/Header/Navbar";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomeContainer from "./components/Content/HomeContainer";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Navbar />} />
          <Route path="/movies" element={<HomeContainer />} />
          <Route path="/tvShows" element={<HomeContainer />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
